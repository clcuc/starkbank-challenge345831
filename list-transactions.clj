(ns my-lib.core
  (:use starkbank.core))

(def private-key-content "-----BEGIN EC PARAMETERS-----\n \n -----END EC PRIVATE KEY-----")
(def user (starkbank.user/project
  "sandbox"
  "5476765568860160"
  private-key-content))
(starkbank.settings/user user)

(def transactions
  (starkbank.transaction/query
  {
    :after "2022-09-10"
    :before "2022-09-16"
  }))

(dorun (map println transactions))
