import requests
import json

##This script successfully sends a POST request to the webhook URL. 
##However, sandbox operations fail to show up as POST requests on 
##the webhook.site web browser GUI. 

webhook_url = 'https://webhook.site/alphanumeric-code'

#data = { 'name': 'testname',
#        'number': '3056' }

data = {
    "invoices": [
        {
            "amount": 400000,
            "due": "2020-11-25T17:59:26.000000+00:00",
            "expiration": 0,
            "name": "Hydro Bank S.A.",
            "taxId": "20.018.183/0001-80",
            "fine": 2.5,
            "interest": 1.3,
            "tags": [
                "Invoice #1234"
            ]
        }
    ]
}

r = requests.post(webhook_url, data=json.dumps(data), headers={'Content-Type': 'application/json'})
