import starkbank
import datetime
import random
from apscheduler.schedulers.blocking import BlockingScheduler
import os

cjob = BlockingScheduler()

private_key_content = """
"""

user = starkbank.Project(
    environment="sandbox",
    id="5476765568860160",
    private_key = private_key_content
)

starkbank.user = user

cnpjList= [['companhia brasileira de distribuicao', '47508411000156'], ['grupo big brasil',
    '30621687000143'], ['daiso brasil comercio e importacao', '14987685001279']]

count = 0

@cjob.scheduled_job('interval', hours=3, next_run_time=datetime.datetime.now(), id='myjobid')
def scheduled_job():
    print('Job executing...')
    global count

    invoices = starkbank.invoice.create([
        starkbank.Invoice(
            amount=20000+10000*random.randrange(150),
            name=cnpjList[tempCompany:=random.randrange(3)][0],
            tax_id=cnpjList[tempCompany][1],
        )
    ])
    
    for i in range(random.randrange(7,12)): 
        invoices += starkbank.invoice.create([
            starkbank.Invoice(
                amount=20000+10000*random.randrange(150),
                name=cnpjList[tempCompany:=random.randrange(3)][0],
                tax_id=cnpjList[tempCompany][1],
            )
        ])
    
    for invoice in invoices:
        print(invoice)
    
    print('Number of invoices: ')
    print(len(invoices))

    count += 1
    if count == 3:
        cjob.remove_job('myjobid')
        os._exit(1)

cjob.start()

#transfers = starkbank.transfer.create([
#    starkbank.Transfer(
#        amount=300,
#        name="Stark Bank S.A.",
#        tax_id="20.018.183/0001-80",
#        bank_code="20018183",
#        branch_code="0001",
#        account_number="6341320293482496",
#        account_type="payment",
#    )
#])
